# FiRe
This project was born out of the necessity of providing enterprises with an all-in-one solution for security monitoring and incident response.
It's core consists of a dockerized Elastic stack and an ElastAlert container.

The main idea is to create a security events pipeline that filters the most relevant content through correlation and High Fidelity Alerts. This curated security events will be analized and turned into security incidentes if they call for such an action.

![Doesn't it look cool!?](https://gitlab.com/protectia/fire/raw/master/Arquitectura-FiRe.png)

### Requirements
* Server with at least 16 GB of ram (AT LEAST, ideally you want to have 32 or more) 
 
### Installation & Set Up

When creating containers from scratch you must first create the volume containing the certificates that enable SSL communication:
```sh
$ docker-compose -f create-certs.yml run --rm create_certs
```

New passwords can & should be defined using the command
``` sh
$ elasticsearch-setup-passwords interactive
``` 

### Usage

#### Adding a data-source (i.e. new device or log type)
* Configure the device (FW, WAF, etc.) to send logs to logstash and set-up an appropiate input/output pipeline in accordance to the [Logstash Documentation](https://www.elastic.co/guide/en/logstash/current/pipeline.html)
* Log into https://server-ip/ and follow standar procedure for [Setting Up Index Patterns](https://www.elastic.co/guide/en/kibana/current/tutorial-define-index.html)
* Create or load visualizations and set-up elastalert to watch for relevant events [LINK PENDING](https://google.com)


### Authors
[@Patricio Parga](https://twitter.com/darkm_is)


